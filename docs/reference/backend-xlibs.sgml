<chapter id="gtkunique-backend-Xlibs">
  <chapterinfo>
    <author>
      <firstname>Emmanuele</firstname>
      <surname>Bassi</surname>
      <affiliation>
        <address>
          <email>ebassi@o-hand.com</email>
        </address>
      </affiliation>
    </author>
  </chapterinfo>

  <title>GtkUnique - Xlibs Backend Protocol</title>

  <para>
    The Xlibs-based GtkUnique protocol uses X as an IPC (Inter-Process
    Communication) channel. It works by setting and retrieving data
    using X properties set on a window. The protocol is implemented by
    the GtkUniqueAppXlibs object available when GtkUnique has been
    compiled with the Xlibs backend.
  </para>

  <para>
    The protocol is composed by three phases: discovery, setup and
    communication.
  </para>

  <section id="discovery">
    <title>Discovery</title>

    <para>
      The discovery phase is performed when the gtk_unique_app_is_running()
      function is used.  In this phase every top-level window is checked
      for the existence of the _GTK_UNIQUE_VERSION and _GTK_UNIQUE_NAME
      properties.  If the _GTK_UNIQUE_VERSION is the same implemented by
      the current version of GtkUniqueAppXlibs and the _GTK_UNIQUE_NAME
      property is set to the same name used when creating the GtkUniqueApp
      instance, then the discovery phase ends.
    </para>

    <para>
      If no top-level window with both _GTK_UNIQUE_VERSION and
      _GTK_UNIQUE_NAME properties matching to the desired values, then no
      previous application has been found and a new instance should be
      created by following the "set up" phase.
    </para>

  </section> <!-- discovery -->

  <section id="setup">
    <title>Setup</title>

    <para>
      In the setup phase the newly created window receives two new string
      properties: the _GTK_UNIQUE_VERSION property containing the version
      of the Xlibs-based protocol; and the _GTK_UNIQUE_NAME property,
      containing the name of the application.  Both these properties must
      be left untouched for the whole existence of the window.
    </para>

    <para>
      The user should listen to the changes of the _GTK_UNIQUE_COMMAND
      property by checking a PropertyNotify event on the window.
    </para>

  </section> <!-- setup -->

  <section id="communication">
    <title>Communication</title>

    <para>
      The real communication between processes is initiated by the
      process calling gtk_unique_app_send_message() (from now on, the
      sender process).  The sender process must find the window of the
      running instance (or receiver process) following the steps in
      the "discovery" phase; once the right window has been found, it
      must set the following properties on that window:
    </para>

    <formalpara>
      <itemizedlist>
        <listitem>
          <para>_GTK_UNIQUE_STARTUP_ID (type: XA_STRING, format: 8),
          containing the startup-id of the startup notification
          specification; this property is optional;</para>
        </listitem>
        <listitem>
          <para>_GTK_UNIQUE_WORKSPACE (type: XA_CARDINAL, format: 32),
          containing the number of the workspace where the sender
          process has been started; this property is required;</para>
        </listitem>
        <listitem>
          <para>_GTK_UNIQUE_COMMAND_DATA (type: XA_STRING, format: 8),
          containing a string to be passed to the receiver process
          as part of the command; this property is optional;</para>
        </listitem>
      </itemizedlist>
    </formalpara>

    <para>
      After setting these properties, the _GTK_UNIQUE_COMMAND (type:
      XA_STRING, format: 8) property must be set using one of these
      command strings:
    </para>

    <formalpara>
      <itemizedlist>
        <listitem>
          <para>new - for creating a new top-level window</para>
        </listitem>
        <listitem>
          <para>open-uri - for opening a new document</para>
        </listitem>
        <listitem>
          <para>activate - for bringing the current window upfront</para>
        </listitem>
        <listitem>
          <para>custom - for custom messages</para>
        </listitem>
      </itemizedlist>
    </formalpara>

    <para>
      Once the sender process has set the _GTK_UNIQUE_COMMAND property it
      should block until a PropertyNotify event has been sent on the
      _GTK_UNIQUE_RESPONSE property.
    </para>

    <para>
      Meanwhile, the receiver process should have received the PropertyNotify
      event on the _GTK_UNIQUE_COMMAND property.  The sender process should
      retrieve the values of the other properties set by the sender process
      and execute the command.  Once the command has been completed, the
      sender process should set the _GTK_UNIQUE_RESPONSE (type: XA_STRING,
      format: 8) property with a response code between the following:
    </para>

    <formalpara>
      <itemizedlist>
        <listitem>
          <para>ok - for a successful completion of the command</para>
        </listitem>
        <listitem>
          <para>cancel - for a user-cancelled completion of the command</para>
        </listitem>
        <listitem>
          <para>abort - for a system-cancelled completion of the command</para>
        </listitem>
      </itemizedlist>
    </formalpara>

    <para>
      The 'fail' and 'invalid' response codes are reserved for error
      conditions inside the GtkUnique library.
    </para>

    <para>
      The sender process should then receive a PropertyNotify event on the
      _GTK_UNIQUE_RESPONSE property, retrieve the value of the property and
      return the code to the user before terminating.
    </para>

  </section> <!-- communication -->

</chapter>
